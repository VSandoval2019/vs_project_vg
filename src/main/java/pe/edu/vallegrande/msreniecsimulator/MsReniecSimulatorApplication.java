package pe.edu.vallegrande.msreniecsimulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsReniecSimulatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsReniecSimulatorApplication.class, args);
    }
}
